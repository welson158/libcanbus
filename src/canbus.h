/********************************************************************************
 *      Copyright:  (C) 2014 EAST
 *                  All rights reserved.
 *
 *       Filename:  canbus.h
 *    Description:  This head file is used to define some macro and struct.
 *
 *        Version:  1.0.0(07/30/2014)
 *         Author:  fulinux <fulinux@sina.com>
 *      ChangeLog:  1, Release initial version on "07/30/2014 10:53:55 AM"
 *                 
 ********************************************************************************/

#ifndef _CANBUS_H_
#define _CANBUS_H_

#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <linux/can.h>

#ifndef MSG_MAXSIZE
#define MSG_MAXSIZE 1785
#endif /* MSG_SIZE */

#ifndef FALSE
#define FALSE 0
#endif
    
#ifndef TRUE
#define TRUE 1
#endif

struct pgn_filter {
    uint32_t pgn;
    uint32_t mask;
};

struct sndlist {
    uint32_t pgn; 
    uint16_t len;
    uint8_t prio;
    uint8_t *msg;
    struct itimerval itv;
};

/*  extended frame, standard frame  is compatible */
typedef struct can_eframe {
    uint32_t pgn; /* Parameter Group Number */
    uint16_t len; /* Total message size, number of bytes(len < 1785). */
    uint8_t *msg; /* message buf address. */
    int32_t debug; /* enable/disable debug print */

    struct _can_private *can_private; /* private data, you don't have to care about it */
}can_eframe_t;

can_eframe_t * can_init(char *candev, uint8_t prio, uint8_t pduf, 
        uint8_t dst, uint8_t src);
int can_getdebug(can_eframe_t *can);
int can_setdebug(can_eframe_t *can, int flag);
int can_getprio(can_eframe_t *can);
int can_setprio(can_eframe_t *can, uint8_t prio);
int can_getpduf(can_eframe_t *can);
int can_setpduf(can_eframe_t *can, uint8_t pduf);
int can_getdst(can_eframe_t *can);
int can_setdst(can_eframe_t *can, uint8_t dst);
int can_getsrc(can_eframe_t *can);
int can_setsrc(can_eframe_t *can, uint8_t src);
int can_send(can_eframe_t *can); 
int can_recv(can_eframe_t *can, struct timeval *timeout); 
int can_setfilter(can_eframe_t *can, struct can_filter *canidfilter, 
        size_t canidlen, struct pgn_filter *pgnfilter, size_t pgnlen); 
int can_cycle2snd(can_eframe_t *can, struct sndlist *sndlist, size_t len);
int can_exit(can_eframe_t *can);

#endif /* _CANBUS_H_ */
