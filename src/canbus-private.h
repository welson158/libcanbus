#ifndef _CANBUS_PRIVATE_H_
#define _CANBUS_PRIVATE_H_

#include <stdint.h>
#include <sys/types.h>
#include <linux/can.h>
#include <pthread.h>

#include "canbus.h"

#define TP_CM 60416
#define TP_DT 60160
#define TP_CM_RTS 16
#define TP_CM_CTS 17
#define TP_CM_EndofMsgACK 19
#define TP_Conn_Abort 255
#define TP_CM_BAM 32

#define TONE 750000 /* T1 */
#define TTWO 1250000 /* T2 */
#define TTHREE TTWO /* T3 */
#define TFOUR 1050000 /* T4 */
#define TR 200000 /* Tr */
#define TH 500000 /* Th */

#define CAN_PF_MASK 0xFF0000 /* PF in PDU head */

/*
 * PDU HEAD
 * ---------------------------------------------
 *  bits |  3  |  1  |  1  |  8  |  8  |  8  |
 * ---------------------------------------------
 *  def  |  P  | EDP | DP  |  PF |  PS |  SA  |
 * ---------------------------------------------
 */
struct _pdu_head {
    uint8_t sa; /* SA is Source address. */
    uint8_t ps; /* PS is PDU Specific/DA is Destination Address. */
    uint8_t pf; /* PF is PDU Format. */
    uint8_t dpedpp; /*  DP is Data Page, EDP is Extended Data Page, P is Priority. */
};

struct _sndlist{
    struct sndlist sndlist;
    struct timeval tv;
};

struct _can_private {
    /*  Socket descriptor */
    int s; 

    int debug; /* enable/disable debug print */
    uint8_t reason; /* Connection Abort Reason. */

    pthread_t pid; /* can_cycle2snd pthread id */
    pthread_cond_t cond;
    pthread_mutex_t mutex; /* mutex lock */

    uint8_t tpackets; /* Total number of packets. */
    uint8_t mpackets; /* Maximum number of packets that can be sent in response to one CTS. */
    uint8_t npacket; /* Next packet number to be sent */

    /* pgn filter */
    int pgnfilter_nr;
    struct pgn_filter *pgnfilter;

    /* send list */
    int sndpkt_nr;
    struct _sndlist *psndlist;

    struct _pdu_head pduhead;
    struct timeval *timeout;
    struct can_frame frame;
};

#endif /* _CANBUS_PRIVATE_H_ */
