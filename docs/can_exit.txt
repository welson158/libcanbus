can_exit(3)
===========


NAME
----
can_exit - struct can_eframe数据结构体的析构函数(或者退出函数).


SYNOPSIS
--------
#include <canbus.h>
int can_exit(can_eframe_t *can);


DESCRIPTION
-----------
can_exit函数释放struct can_eframe数据结构体中的资源.


RETURN VALUE
------------
成功返回0, 失败返回-1.


EXAMPLE
-------
[source,c]
-------------------
struct can_eframe *can;

can = can_init("vcan0", 6, 0, 0xE5, 0xF4);
if(NULL == can){
    perror("CAN INIT");
    return -1;
}

...

can_exit(can);
-------------------

SEE ALSO
--------
linkmb:can_init[3]


AUTHORS
-------
fulinux<fulinux@sina.com>
