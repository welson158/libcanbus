/*********************************************************************************
 *      Copyright:  (C) 2014 EAST
 *                  All rights reserved.
 *
 *       Filename:  main.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(07/30/2014)
 *         Author:  fulinux <fulinux@sina.com>
 *      ChangeLog:  1, Release initial version on "07/30/2014 01:52:51 PM"
 *                 
 ********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>
#include <canbus.h>

/********************************************************************************
 *  Description:
 *   Input Args:
 *  Output Args:
 * Return Value:
 ********************************************************************************/
int main (int argc, char **argv)
{
    uint8_t *message;
    struct sndlist sndlist[4];
    can_eframe_t *can;

    can = can_init("vcan0", 6, 0, 0xE5, 0xF4);
    if(NULL == can){
        perror("CAN INIT");
        return -1;
    }

    if(-1 == can_setdebug(can, TRUE)){
        perror("SET DEBUG");
        return -1;
    }

    message = (uint8_t *)malloc(512 * sizeof(uint8_t));
    message[0] = 1;
    message[1] = 2;
    message[5] = 5;
    message[6] = 6;
    message[11] = 11;
    message[22] = 22;
    message[33] = 33;
    message[44] = 44;
    message[400] = 40;
    message[500] = 50;
    message[510] = 31;
    message[511] = 51;

    sndlist[0].pgn = 256;
    sndlist[0].len = 4;
    sndlist[0].prio = 6;
    sndlist[0].msg = message;
    sndlist[0].itv.it_value.tv_sec = 0;
    sndlist[0].itv.it_value.tv_usec = 0;
    sndlist[0].itv.it_interval.tv_sec = 0;
    sndlist[0].itv.it_interval.tv_usec = 200000; /* 200ms */

    /* 该报文在50ms后开始周期性发送, 周期是300ms. */
    sndlist[1].pgn = 512;
    sndlist[1].len = 17;
    sndlist[1].prio = 6;
    sndlist[1].msg = message + 30;
    sndlist[1].itv.it_value.tv_sec = 0;
    sndlist[1].itv.it_value.tv_usec = 500000; /* 500ms */
    sndlist[1].itv.it_interval.tv_sec = 0;
    sndlist[1].itv.it_interval.tv_usec = 300000; /* 300ms */

    /* 该报文在100ms后开始周期性发送, 周期是400ms. */
    sndlist[2].pgn = 768;
    sndlist[2].len = 8;
    sndlist[2].prio = 6;
    sndlist[2].msg = message + 399;
    sndlist[2].itv.it_value.tv_sec = 1;
    sndlist[2].itv.it_value.tv_usec = 0;
    sndlist[2].itv.it_interval.tv_sec = 0;
    sndlist[2].itv.it_interval.tv_usec = 400000; /* 400ms */

    sndlist[3].pgn = 1024;
    sndlist[3].len = 13;
    sndlist[3].prio = 6;
    sndlist[3].msg = message + 500;
    sndlist[3].itv.it_value.tv_sec = 1;
    sndlist[3].itv.it_value.tv_usec = 500000;
    sndlist[3].itv.it_interval.tv_sec = 0;
    sndlist[3].itv.it_interval.tv_usec = 500000; /* 500ms */

    can_cycle2snd(can, sndlist, sizeof(sndlist));

    /* Don't use sleep */
    for(;;){
        pause();
    }

    if(-1 == can_exit(can)){
        perror("CAN EXIT");
        exit(-1);
    }

    return 0;
} /* ----- End of main() ----- */
