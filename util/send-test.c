/*********************************************************************************
 *      Copyright:  (C) 2014 EAST
 *                  All rights reserved.
 *
 *       Filename:  main.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(07/30/2014)
 *         Author:  fulinux <fulinux@sina.com>
 *      ChangeLog:  1, Release initial version on "07/30/2014 01:52:51 PM"
 *                 
 ********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>
#include <canbus.h>

/********************************************************************************
 *  Description:
 *   Input Args:
 *  Output Args:
 * Return Value:
 ********************************************************************************/
int main (int argc, char **argv)
{
    can_eframe_t *can;

    can = can_init("vcan0", 6, 0, 0xE5, 0xF4);
    if(NULL == can){
        perror("CAN INIT");
        return -1;
    }

    if(-1 == can_setdebug(can, TRUE)){
        perror("SET DEBUG");
        return -1;
    }

    can->msg[0] = 1;
    can->msg[1] = 2;
    can->msg[2] = 3;
    can->msg[8] = 8;
    can->msg[9] = 9;
    can->msg[10] = 10;
    can->msg[499] = 55;
    can->msg[500] = 55;
    can->msg[509] = 10;
    can->msg[510] = 11;
    can->msg[511] = 12;

    for(;;){
        can->pgn = 256;
        can->len = 8;
        if(can_send(can) == -1){
            perror("CAN SEND ");
            //break;
        }

        can->pgn = 512;
        can->len = 41;
        if(can_send(can) == -1){
            perror("CAN SEND ");
            //break;
        }

        can->pgn = 768;
        can->len = 3;
        if(can_send(can) == -1){
            perror("CAN SEND");
            //break;
        }

        can->pgn = 1024;
        can->len = 33;
        if(can_send(can) == -1){
            perror("CAN SEND");
            //break;
        }

        sleep(2);
    }

    if(-1 == can_exit(can)){
        perror("CAN EXIT");
        exit(-1);
    }

    return 0;
} /* ----- End of main() ----- */
